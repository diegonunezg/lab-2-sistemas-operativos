#include <stdio.h>
#include <pthread.h>

// Función a ejecutar por cada una de las hebras creadas, estas imprimirán su nombre
void *nueva_hebra(void *nombre_hilo) {
    char *id = (char*)nombre_hilo;
    printf("%s creado!\n", id);
    return NULL;
}

int main() {

    // Se van a crear 3 hebras
    pthread_t hebras[3];
    char *nombres[] = {"H1", "H2", "H3"};
    
    for (int id = 0; id < 3; id++) {
        // La función pthread_create crea 3 hebras, cada una con su nombre, que ejecutarán la función nueva_hebra()
        int ret = pthread_create(&hebras[id], NULL, nueva_hebra, nombres[id]);
        if (ret) {
            printf("Error al crear %s", nombres[id]);
        }
    }

    // El programa principal espera a que todas las hebras terminen, para luego, terminar él.
    for (int i = 0; i < 3; i++) {
        pthread_join(hebras[i], NULL);
    }

    return 0;
}