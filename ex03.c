#include <stdio.h>
#include <pthread.h>

// Función de la hebra 3
void *hilo_h3(void *nombre_hebra) {
    char *nombre = (char*)nombre_hebra;
    printf("%s creado\n", nombre);

    // La hebra 3 realiza su trabajo, por ejemplo una suma
    int x = 1 + 1;

    // La hebra 3 termina y retorna
    printf("%s termina\n", nombre);
    return NULL;
}

// Función de la hebra 2
void *hilo_h2(void *nombre_hebra) {
    char *nombre = (char*)nombre_hebra;
    printf("%s creado\n", nombre);

    // La hebra 2 realiza su trabajo, en este caso es crear una hebra hija llamada H3
    pthread_t hebra3;
    int ret = pthread_create(&hebra3, NULL, hilo_h3, "H3");
    if (ret) {
        printf("Error al crear la hebra 3");
        pthread_exit(NULL);
    }

    // La hebra 2 espera a que la hebra 3 termine
    pthread_join(hebra3, NULL);

    // La hebra 2 termina
    printf("%s termina\n", nombre);
    return NULL;
    
}

// Función de la hebra 1
void *hilo_h1(void *nombre_hebra) {
    char *nombre = (char*)nombre_hebra;
    printf("%s creado\n", nombre);

    // La hebra 1 realiza su trabajo, en este caso es crear una hebra hija llamada H2
    pthread_t hebra2;
    int ret = pthread_create(&hebra2, NULL, hilo_h2, "H2");
    if (ret) {
        printf("Error al crear la hebra 2");
        pthread_exit(NULL);
    }

    // La hebra 1 espera a que la hebra 2 termine
    pthread_join(hebra2, NULL);

    // La hebra 1 termina
    printf("%s termina\n", nombre);
    return NULL;
}

int main() {
    printf("Programa principal comienza\n");
    // El programa principal crea una hebra H1, que ejecutará la función hilo_h1
    pthread_t hebra1;
    int ret = pthread_create(&hebra1, NULL, hilo_h1, "H1");
    if (ret) {
        printf("Error creando hilo hijo H1\n");
        return 1;
    }
    // El programa principal espera a que la hebra 1 termine
    pthread_join(hebra1, NULL);

    printf("Programa principal termina\n");
    return 0;
}
