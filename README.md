# Lab 2 Sistemas Operativos



## Resolución del primer laboratorio del ramo Sistemas Operativos y Redes

---
Los ejercicios son los siguientes: 

1.	Implementar solución en C Linux que mediante el uso de hebras el programa principal cree exactamente 8 “procesos ligeros” en total, donde cada proceso muestra su nombre (el nombre corresponde a un parámetro que la función de cada hebra recibe). Por ejemplo, “Hebra1” para la hebra 1, “Hebra2” para la hebra 2, y así sucesivamente.



2.	Implementar solución en C Linux que mediante uso de hebras el programa principal cree exactamente 3 procesos ligeros H1, H2 y H3 (las hebras hijas no crean nuevas hijas), donde cada hebra hija se identifica al momento de ser creado (¡envía un mensaje tal como “H1 creado!”).



3.	Implementar solución en C Linux que, mediante uso de hebras, el programa principal cree una hebra hija H1 la cual crea una hebra hija H2, y H2 crea otra hebra hija H3. Cada hebra hija se identifica al momento de ser creado y al momento de terminar su ejecución. Por ejemplo, “H1 creado” y “H1 termina”, donde cada hebra madre espera a que su hija termine (puede usar pthread_join(..) para que una hebra espera a que otra termine.

