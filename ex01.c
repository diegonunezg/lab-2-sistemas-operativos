#include <stdio.h>
#include <pthread.h>

// Función a ejecutar por cada una de las hebras creadas, estas imprimirán su nombre
void *mostrar_nombre(void *nombre_hilo) {
    char *id = (char*)nombre_hilo;
    printf("Hola soy la %s\n", id);
    return NULL;
}

int main() {

    // Se van a crear 8 hebras
    pthread_t hebras[8];
    char *nombres[] = {"Hebra 1", "Hebra 2", "Hebra 3", "Hebra 4", "Hebra 5", "Hebra 6", "Hebra 7", "Hebra 8"};
    
    for (int id = 0; id < 8; id++) {
        // La función pthread_create crea 8 hebras, cada una con su nombre, que ejecutarán la función mostrar_nombre()
        int ret = pthread_create(&hebras[id], NULL, mostrar_nombre, nombres[id]);
        if (ret) {
            printf("Error al crear %s", nombres[id]);
        }
    }

    // El programa principal espera a que todas las hebras terminen, para luego, terminar él.
    for (int i = 0; i < 8; i++) {
        pthread_join(hebras[i], NULL);
    }

    return 0;
}